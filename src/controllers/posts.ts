import { Response,Request, NextFunction } from "express";
import axios,{AxiosResponse} from "axios";


interface Post {
    userId:number,
    id:number,
    title:string,
    body:string
}

// recuperer tous les posts
const getPosts=async(req:Request,res:Response,next:NextFunction)=>{
    let resultat:AxiosResponse= await axios.get(`https://jsonplaceholder.typicode.com/posts`);
    let posts:[Post]=resultat.data;

    return res.status(200).json({message:posts})
}

//recupere un seul post

const getPost=async(req:Request,res:Response,next:NextFunction)=>{
    let id: string = req.params.id; 
    let resultat:AxiosResponse= await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
    let post:Post= resultat.data
    return res.status(200).json({message:post})

}

// ajouter le post
const addPost=async(req:Request,res:Response,next:NextFunction)=>{
    
    let title:string=req.params.title;
    let body:string=req.params.body;

    let response:AxiosResponse=await axios.post(`https://jsonplaceholder.typicode.com/posts`,{title,body}   )
    return res.status(200).json({message:response.data})
}

// modifier le post
const updatePost=async(req:Request,res:Response,next:NextFunction)=>{
    let id:string=req.params.id;
    let title:string=req.body.title ?? null;
    let body:string=req.body.body ?? null;

    let response:AxiosResponse=await axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`,{...(title && {title}),...(body && {body})});
    return res.status(200).json({message:response.data});
}
// supprime post

const deletePost=async(req:Request,res:Response,next:NextFunction)=>{
    let id:string=req.params.id;
    let response:AxiosResponse=await axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);
    return res.status(200).json({message:" le post est supprime "})
}

export default {getPost,getPosts,updatePost,deletePost,addPost}