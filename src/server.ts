import http from 'http'
import express,{Express} from 'express'
import morgan from 'morgan'
import router from './routes/post'



const chemin:Express=express();

chemin.use(morgan('dev'))
chemin.use(express.urlencoded({extended:false}))
chemin.use(express.json())

chemin.use((req,res,next)=>{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers','origin, X-Requested-With,Content-Type,Accept, Authorization');
    if(req.method==='OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'GET PATCH DELETE POST');
        return res.status(200).json({});
    }
    next();
});    

chemin.use('/',router);


chemin.use((req, res, next) => {
    const error = new Error('not found');
    return res.status(404).json({
        message: error.message
    });
});

/** Server */
const httpServer = http.createServer(chemin);
const PORT: any = process.env.PORT ?? 6060;
httpServer.listen(PORT, () => console.log(`The server is running on port ${PORT}`));