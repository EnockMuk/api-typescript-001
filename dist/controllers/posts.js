"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
// recuperer tous les posts
const getPosts = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    let resultat = yield axios_1.default.get(`https://jsonplaceholder.typecode.com/posts`);
    let posts = resultat.data;
    return res.status(200).json({ message: posts });
});
//recupere un seul post
const getPost = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    let id = req.params.id;
    let resultat = yield axios_1.default.get(`https://jsonplaceholder.typecode.com/posts`);
    let post = resultat.data;
    return res.status(200).json({ message: post });
});
// ajouter le post
const addPost = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    let title = req.params.title;
    let body = req.params.body;
    let response = yield axios_1.default.post(`https://jsonplaceholder.typecode.com/posts`, { title, body });
    return res.status(200).json({ message: response.data });
});
// modifier le podt
const updatePost = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    let id = req.params.id;
    let title = (_a = req.body.title) !== null && _a !== void 0 ? _a : null;
    let body = (_b = req.body.body) !== null && _b !== void 0 ? _b : null;
    let response = yield axios_1.default.put(`https://jsonplaceholder.typecode.com/posts/${id}`, Object.assign(Object.assign({}, (title && { title })), (body && { body })));
    return res.status(200).json({ message: response.data });
});
// supprime post
const deletePost = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    let id = req.params.id;
    let response = yield axios_1.default.delete(`https://jsonplaceholder.typecode.com/posts/${id}`);
    return res.status(200).json({ message: " le post est supprime " });
});
exports.default = { getPost, getPosts, updatePost, deletePost, addPost };
